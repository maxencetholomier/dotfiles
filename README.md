# Dotfiles

This repository contains the dotfiles of the software I'm using.

I will make frequent modifications to this repository.

Everything you can find here is heavily commented.

I highly recommend **not** installing my entire configuration. Instead, feel free to selectively pick what interests you.

## Dependancy

Git need to be installed.

## Installation script

Clone this repository under $HOME/.dotfiles.

Then run :

```bash
cd $HOME/.dotfiles
./install.bash
```

### Joplin

I use Joplin mostly to get my computer's notes on my phone.

At each installation :

- I'm disabling JoplinBackup from GUI (Tools -> Options -> Modules)
- I'm removing the JoplinBackup folder automatically created in my $HOME directory

Hope's to have a proper config file soonly implemented. The current one has display the API.

### Default Folders

The default folders on Linux (home, desktop, download, etc.) are configured using XDG variables.

I can't automate this part as it depends on the hardware configuration (e.g., a computer with two SSDs).

Open ~/.config/user-dirs.dirs and adjust the variables according to your preferences.

## Credits

Much of my configuration is inspired by:

- [Chewie](https://github.com/Chewie)
- [Chris\@machine](https://github.com/ChristianChiarulli)
- [Dereck Taylor](https://gitlab.com/dwt1)
- [Gavin Freeborn](https://github.com/Gavinok)
- [Leeren](https://github.com/leeren)
- [Rob Muhlestein](https://github.com/rwxrob)
- [Luke Smith](https://lukesmith.xyz/)
- [The Primeagen](https://github.com/ThePrimeagen/.dotfiles)
- [Tjdevries](https://github.com/tjdevries)
