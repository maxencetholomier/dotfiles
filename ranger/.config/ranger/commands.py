import os
import subprocess
import ranger.api
import ranger.core.fm
import ranger.ext.signals
from subprocess import PIPE

class z(ranger.api.commands.Command):
    """
    :z

    Jump around with zoxide (z)
    """
    def execute(self):
        results = self.query(self.args[1:])

        input_path = ' '.join(self.args[1:])
        if not results and os.path.isdir(input_path):
            self.fm.cd(input_path)
            return

        if not results:
            return

        if os.path.isdir(results[0]):
            self.fm.cd(results[0])

    def query(self, args):
        try:
            zoxide = self.fm.execute_command(f"zoxide query {' '.join(self.args[1:])}",
                                             stdout=PIPE
                                             )
            stdout, stderr = zoxide.communicate()

            if zoxide.returncode == 0:
                output = stdout.decode("utf-8").strip()
                return output.splitlines()
            elif zoxide.returncode == 1: # nothing found
                return None
            elif zoxide.returncode == 130: # user cancelled
                return None
            else:
                output = stderr.decode("utf-8").strip() or f"zoxide: unexpected error (exit code {zoxide.returncode})"
                self.fm.notify(output, bad=True)
        except Exception as e:
            self.fm.notify(e, bad=True)

    def tab(self, tabnum):
        results = self.query(self.args[1:])
        return ["z {}".format(x) for x in results]

class fzf_alt_c(ranger.api.commands.Command):
    """
    :fzf
    Find a directory using fzf.
    """
    def execute(self):
        fzf_exclusion_list = os.getenv("FZF_EXCLUSION_LIST")
        fzf_exclusion_list = fzf_exclusion_list.replace('\\\\', '\\')
        tmux = os.getenv("TMUX")
        if tmux :
            fzf_tmux_opts = os.getenv("FZF_TMUX_OPTS")
            fzf_alt_c_opts = os.getenv("FZF_ALT_C_OPTS")
            command=f"find . -mindepth 1 -type d | grep -v '{fzf_exclusion_list}' | fzf-tmux {fzf_tmux_opts} {fzf_alt_c_opts}"
        else:
            command=f"find . -mindepth 1 -type d | grep -v '{fzf_exclusion_list}' | fzf"

        fzf = self.fm.execute_command(command, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.decode('utf-8').rstrip('\n'))
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.select_file(fzf_file)
