# ==============================================================================
#  Bash's Parameters
# ==============================================================================

case $- in
*i*) ;; # interactive
*) return ;;
esac

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

stty -ixon # Disable Freezing with C-S if bash is called interactively

set -o vi                     # Use Vi keys

shopt -s direxpand            # Expand variable path using tab
shopt -s histappend           # Do not erase history, append new commands at the bottom
shopt -s checkwinsize         # Check the window size after each command and resize, if necessary,

bind '"\C-l":"clear\015"'     # This is not default in vi mode

# Prevent (big) mistake(s)
export HISTIGNORE="rm*:*--force*:"
export HISTIGNORE+="vif*:vit*:vig*"

# ==============================================================================
#  Variables
# ==============================================================================

# System
export BROWSER="firefox"
export CC="gcc"
export EDITOR="nvim"
export HISTCONTROL=ignoreboth:erasedups # Reload History after typing a command (usefull while using tmux sessions)
export HISTFILESIZE=100000 # Size of bash_history
export HISTSIZE=100000 # Cache of a session that will be written in bash_history at exit
export PATH="${PATH}":"${HOME}"/.local/bin # Some python packages are here and not sourced on debian
export PROMPT_COMMAND="history -a; history -c; history -r;${PROMPT_COMMAND}" # Avoid duplicate in history
export QT_QPA_PLATFORMTHEME=qt5ct # Graphical API variables (mostly used for darkmode for certain application as Wireshark)
export SUDO_EDITOR="nvim"
export TERMINAL="kitty"
export VISUAL="nvim"

if command -v ov >& /dev/null ; then
	export PAGER="ov"
else
  export PAGER="less"
fi

# User
export remoterepos=~/remoterepos
export dotfiles=~/.dotfiles
export logiciels=~/logiciels

export notes="${remoterepos}"/notes
export projects="${remoterepos}"/projects
export tools="${remoterepos}"/tools
export zettelkasten="${remoterepos}"/zettelkasten/notes


# Tools  (FZF, RG , ...)
export TOOLS_EXCLUSION_FOLDERS=( \
	'.gdfuse' \
	'.git' \
	'__pycache__' \
	'_build' \
	'deps' \
	'node_modules' \
	'priv' \
	'venv' \
  '.elixir_ls' \
)

export TOOLS_EXCLUSION_FILES_EXTENSION=( \
	'aac' \
	'flac' \
	'gz' \
	'jpeg' \
	'jpg' \
	'jpg' \
	'mp3' \
	'pdf' \
	'png' \
	'rar' \
	'tags' \
	'tar' \
	'wav' \
	'wep' \
	'xlsx' \
	'zip' \
)

export TOOLS_EXCLUSION_FILES_NAME=( \
	'erl_crash.dump' \
)

TOOLS_EXCLUSION_FILES_EXTENSION_REGEX=("${TOOLS_EXCLUSION_FILES_EXTENSION[@]/%/$}")
TOOLS_EXCLUSION_FILES_EXTENSION_REGEX=("${TOOLS_EXCLUSION_FILES_EXTENSION_REGEX[@]/#/\\.}")
TOOLS_EXCLUSION_FILES_NAME_REGEX=("${TOOLS_EXCLUSION_FILES_NAME[@]/%/$}")
# TOOLS_EXCLUSION_FILES_REGEX=(${TOOLS_EXCLUSION_FILES_EXTENSION_REGEX[@]} ${TOOLS_EXCLUSION_FILES_NAME_REGEX[@]})
# read -ar TOOLS_EXCLUSION_FILES_REGEX <<< "${TOOLS_EXCLUSION_FILES_EXTENSION_REGEX[*]} ${TOOLS_EXCLUSION_FILES_NAME_REGEX[*]}"
mapfile -t TOOLS_EXCLUSION_FILES_REGEX < <(printf "%s\n" "${TOOLS_EXCLUSION_FILES_EXTENSION_REGEX[@]}" "${TOOLS_EXCLUSION_FILES_NAME_REGEX[@]}")

TOOLS_EXCLUSION_FOLDERS_REGEX=("${TOOLS_EXCLUSION_FOLDERS[@]//./\\.}")
TOOLS_EXCLUSION_FOLDERS_REGEX=("${TOOLS_EXCLUSION_FOLDERS_REGEX[@]/#/^\\./}")


export TOOLS_EXCLUSION_FILES_REGEX
export TOOLS_EXCLUSION_FOLDERS_REGEX

TOOLS_EXCLUSION_FOLDERS_GLOBBING=("${TOOLS_EXCLUSION_FOLDERS[@]/#/*}")
# TOOLS_EXCLUSION_FILES_GLOBBING=(${TOOLS_EXCLUSION_FILES_EXTENSION[@]/#/*.} ${TOOLS_EXCLUSION_FILES_NAME[@]/#/*})
mapfile -t TOOLS_EXCLUSION_FILES_GLOBBING < <(printf "%s\n" "${TOOLS_EXCLUSION_FILES_EXTENSION[@]/#/*.}" "${TOOLS_EXCLUSION_FILES_NAME[@]/#/*}")

export TOOLS_EXCLUSION_FILES_GLOBBING
export TOOLS_EXCLUSION_FOLDERS_GLOBBING

# Theme
if [[ ${XDG_CURRENT_DESKTOP} =~ 'GNOME' ]] ; then
    GNOME_THEME=$(gsettings get org.gnome.desktop.interface color-scheme)
    export GNOME_THEME
else
		GNOME_THEME=""
    export GNOME_THEME
fi

#=======================================================================
# Tools Configuration
#=======================================================================

# ASDF (for elixir) -----------------------------------------------------------

if [[ -d ~/.asdf ]]; then
  source "${HOME}/.asdf/asdf.sh"
  source "${HOME}/.asdf/completions/asdf.bash"
  export KERL_CONFIGURE_OPTIONS="--disable-debug --without-javac --with-ssl=/usr/local/lib/openssl-1.1.1"
  export KERL_BUILD_DOCS=yes
fi

# Bash ------------------------------------------------------------------------

if [[ ${SHELL} == "/bin/bash" ]] ; then
  [[ -f /etc/bashrc ]] && . /etc/bashrc # Source global definitions
  if [[ -f /usr/share/bash-completion/bash_completion ]]; then # Enable bashcompletion features in interactive shells
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

[[ -f ~/.local_configuration.bash ]] && source "${HOME}/.local_configuration.bash"
[[ -f "${tools}"/complete_scripts ]] && source "${tools}"/complete_scripts

# Bat -------------------------------------------------------------------------

[[ ${GNOME_THEME} =~ "dark" ]] && export BAT_THEME="gruvbox-dark" || export BAT_THEME="gruvbox-light"

if command -v bat >& /dev/null ; then
	export BATPAGER='ov'
  export MANPAGER="sh -c \"col -bx | bat -l man -p --theme=${BAT_THEME}\""
else
  export MANPAGER="less"
fi

# Cargo -----------------------------------------------------------------------

[[ -d  "${HOME}"/.cargo/bin ]] && export PATH="${PATH}":"${HOME}"/.cargo/bin # Some packages are here and not sourced on debian


# FZF -------------------------------------------------------------------------

[[ ! "${PATH}" == */home/${USER}/.config/.fzf/bin* ]] && PATH="${PATH:+${PATH}:}/home/${USER}/.config/.fzf/bin"

if command -v fzf >& /dev/null; then

	# TODO: replace that by source $(fzf --bash) when the fzf will be in right version on debian

  source "/home/${USER}/.config/.fzf/shell/completion.bash" 2> /dev/null

  if [[ -f  "/home/${USER}/.config/.fzf/shell/key-bindings.bash" ]]; then
      source "/home/${USER}/.config/.fzf/shell/key-bindings.bash"
  fi

  _fzf_exclusion_list () {

			# not_to_include=(${TOOLS_EXCLUSION_FOLDERS_REGEX[@]} ${TOOLS_EXCLUSION_FILES_REGEX[@]})
			mapfile -t not_to_include < <(printf "%s\n" "${TOOLS_EXCLUSION_FOLDERS_REGEX[@]}" "${TOOLS_EXCLUSION_FILES_REGEX[@]}")

      folders_to_exclude=""
      for ni in "${not_to_include[@]}"; do
          if [[ -z "${folders_to_exclude}" ]] ; then
              folders_to_exclude="${ni}"
          else
              folders_to_exclude="${folders_to_exclude}\\|${ni}"
          fi
      done
      echo "${folders_to_exclude}"
  }

	FZF_EXCLUSION_LIST=$(_fzf_exclusion_list) # Personnal variable only used in nvim
	export FZF_EXCLUSION_LIST

	if [[ ${GNOME_THEME} =~ "dark" ]]; then
		export FZF_DEFAULT_OPTS="--color=bg+:#ddd3ac,fg+:#000000,hl+:#f25756,hl:#f25757"
		export FZF_DEFAULT_OPTS="--color=preview-border:#727272,border:#727272,separator:#ebdbb2,hl+:#d11010,hl:#d11010"
	else
    # set highlighting as light gray
		export FZF_DEFAULT_OPTS="--color=hl+:#d11011,hl:#d11010,bg+:#ddd3ac,fg+:#000000,border:#292929"
		# export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --bind 'ctrl-r:transform-query:echo -n {q}; tmux show-buffer'"
		export FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --bind 'ctrl-]:transform-query:echo -n {q}; tmux show-buffer'"
	fi


	export FZF_DEFAULT_COMMAND="find . -type f | grep -v \"$(_fzf_exclusion_list)\""

	if [[ -n ${TMUX} ]]; then
		export FZF_TMUX='1'
		export FZF_TMUX_OPTS="-p90%,80%"
		export FZF_CTRL_T_OPTS="--preview 'bat -p --color=always --theme=${BAT_THEME} {}' --layout=default --preview-window=top:wrap"
		export FZF_ALT_C_OPTS="--layout=default --preview 'tree -C {}' --preview-window=top:wrap"
	else
		export FZF_CTRL_T_OPTS=""
	fi

	export FZF_CTRL_R_OPTS="--history=${HOME}/.bash_history --history-size=100000"

fi

# GO --------------------------------------------------------------------------

if command -v go >& /dev/null ; then
	export GOPATH="${HOME}/.go"
	go env -w GOPATH="${HOME}/.go"
	export PATH="${PATH}:${GOPATH}/bin"
fi

# Kitty ----------------------------------------------------------------------

if [[ -n ${KITTY_PID} ]]; then
	# Remove the annoying path title in windows decoration while using kitty
	export PROMPT_COMMAND='echo -en "\033]0;Kitty\a"'
fi


# Less ------------------------------------------------------------------------

if command -v less >& /dev/null ; then
	export LESS="-R --mouse --wheel-lines=3 --RAW-CONTROL-CHARS"
  export LESS_TERMCAP_mb=$'\e[1;31m'
  export LESS_TERMCAP_md=$'\e[1;36m'
  export LESS_TERMCAP_me=$'\e[0m'
  export LESS_TERMCAP_se=$'\e[0m'
  export LESS_TERMCAP_so=$'\e[1;44;33m'
  export LESS_TERMCAP_ue=$'\e[0m'
  export LESS_TERMCAP_us=$'\e[1;32m'
fi

# Mason -----------------------------------------------------------------------

[[ -d  ~/.local/share/nvim/mason/bin ]] && export PATH="${PATH}":~/.local/share/nvim/mason/bin

# RG --------------------------------------------------------------------------

if command -v rg >& /dev/null ; then
	printf "%s\n" "${TOOLS_EXCLUSION_FILES_GLOBBING[@]}"  > ~/.rgignore
	printf "%s\n" "${TOOLS_EXCLUSION_FOLDERS_GLOBBING[@]}" >> ~/.rgignore
fi

# Zoxide -----------------------------------------------------------------------

if command -v zoxide >& /dev/null ; then
	eval "$(zoxide init bash)"
	alias cd='z'
fi

#=======================================================================
# Alias
#=======================================================================

# I prefer to use scripts over aliases.
# Scripts are available in any shell (ex: in vim with :.!)
#   The only exception is cd as I want to be cd in script and not zoxyde

alias man="LANG=en man" # Man (man page should be in english not french)

if command -v rg >& /dev/null ; then
	alias grep='rg -i'
else
	alias grep='grep --color=auto -i'
fi
