"=============================================================================
" Basic Configs
"==============================================================================

"{{{

" Configuration file

filetype plugin indent on
filetype plugin on                    " Automatically detect file type
set exrc                              " Load .vimrc if there is one in current folder
set nohidden                          " Do not switch buffer if there are unsaved modification

" Paths and Files

set path=.,,                          " Only index the current file/folder (I add a vimrc for each project)

" Appearance

syntax on                             " Enable syntax highlighting
set background=dark                   " Fix the background as dark
set nowrap                            " Don't wrap the text when the window is to small to display it
set showmatch                         " Show the matching braces/parentheses

" Alarms

set noerrorbells                      " Disable bell alarms
set novisualbell                      " Disable visual alarm

" Compatibility and System Interaction

set encoding=utf-8                    " Set the encoding to UTF-8, specially useful on Windows machine
set nocompatible                      " Disable vi compatibly to use recent features
set timeoutlen=1000                   " Set timeout for mapping such as gf
set ttimeoutlen=100                   " Set timeout for key code such as escape (time out for visual mode is very annoying)
if !has('nvim')
  set paste
endif

" Backup

set nobackup                          " Git take this part
set noswapfile                        " Fuck swap file, really really hard
set nowritebackup                     " Git take this part

" Navigation

set mouse=a                           " Enable mouse selection
set nu                                " Display line number
set scrolloff=8                       " Enable scrolling when the bottom of the page is reach
if !has('nvim')
  set ttymouse=xterm2                 " Enable pane resizing mouse
endif
set grepprg=rg\ --vimgrep\ --ignore-file\ ~/.rgignore  " grep don't have config file to ignore carbage file

" Research

set ignorecase                        " Ignore case while searching
set smartcase                         " Until typing a Capital letter
set hlsearch                          " Highlight the results after the search
set incsearch                         " Display results while searching

" Text Formatting

set tabstop=2                         " Set existing tabulation to 2 spaces long
set softtabstop=2                     " Set the number of spaces to 2 when pressing tab
set shiftwidth=2                      " Set the number of spaces to 2 when pressing < or >
set tw=0                              " Don't wrap line when the number of character reach 80 character
set expandtab                         " Replace tabulation with space
set list listchars=nbsp:!             " Show non-breaking space
set list!

" Spelling

set spelllang=fr                      " Well, I'm french.

" Autocompletion

set wildmenu                          " Enabling completions in command mode
set wildignorecase                    " Disable case sensitiveness while using wildmenu
set wildcharm=<tab>                   " Trigger completion with <tab> with map, nmap ....

" Netrw

let g:netrw_banner= 0                 " Remove the help banner
let g:netrw_use_errorwindow = 0       " Disable Error windows, very annoying
let g:netrw_preview= 1                " Keep position of last line

" Color

let g:gnome_color_scheme = system("gsettings get org.gnome.desktop.interface color-scheme")

if g:gnome_color_scheme =~ 'dark'
  set background=dark
else
  set background=light
  colorscheme morning " this differ from nvim, vim8 and vim9
  highlight Normal guibg=NONE
  highlight Normal ctermbg=NONE
  highlight NonText guibg=NONE
  highlight NonText ctermbg=NONE
  highlight EndOfBuffer guibg=NONE
  highlight EndOfBuffer ctermbg=NONE
endif

"hi Normal guibg=NONE ctermbg=NONE

"}}}

"==============================================================================
" Commands and Functions
"==============================================================================

"{{{

" Reload config

command! Reload :source $MYVIMRC | echo "Configuration Reload!"

" Use git jump to populate git-jump, copy from Romainl
command! -bar -nargs=* GJump cexpr system('git jump --stdout ' . expand(<q-args>))

" Display in tab only the filename (Copy from jamescherti config)

function! TabLabel(tabnr) abort
  let l:bufnr = tabpagebuflist(a:tabnr)[tabpagewinnr(a:tabnr) - 1]
  let l:modified = 0
  if getbufvar(l:bufnr, '&modified')
    let l:modified = 1
  endif
  let l:tablabel = ''
  let l:custom_tablabel = gettabvar(a:tabnr, 'tablabel', '')
  if empty(l:custom_tablabel)
    let l:bufname = bufname(l:bufnr)
    if empty(l:bufname)
      let l:tablabel = empty(&buftype) ? 'No Name' : '<' . &buftype . '>'
    else
      let l:tablabel = fnamemodify(l:bufname, ':t')
      if l:bufname =~ "fugitive" && empty(l:tablabel)
        let l:tablabel = "G status"
      elseif empty(l:tablabel)
        let l:tablabel = "No Name"
      endif
    endif
  else
    let l:tablabel .= l:custom_tablabel
  endif
  if l:modified
    let l:tablabel .= '*'
  endif
  return l:tablabel
endfunction

" Replace home directory with tilde (usefull in tagstack using g<C-]>) (Copy from jamescherti config)

function! ReplaceHomeWithTilde(path) abort
  let l:path = fnamemodify(a:path, ':p')
  let l:path_sep = (!exists('+shellslash') || &shellslash) ? '/' : '\'
  let l:home = fnamemodify('~', ':p')

  if l:path[0:len(l:home)-1] ==# l:home
    return '~' . l:path_sep . l:path[len(l:home):]
  elseif l:path == l:home
    return '~' . l:path_sep
  endif

  return l:path
endfunction

" Quickly renamed tab (Copy from jamescherti config)
" TODO: make it work for fugitive windows

function! TabLine() abort
  let l:tabline = ''
  for l:num in range(1, tabpagenr('$'))
    let l:tabline .= (l:num != tabpagenr()) ? '%#TabLine#' : '%#TabLineSel#'
    let l:tabline .= '%' . l:num . 'T %{TabLabel(' . l:num . ')} '
  endfor
  let l:tabline .= '%#TabLineFill#%T%='
  let l:tabline .= repeat('%#TabLine#%999X[X]', l:num > 1)
  return l:tabline
endfunction

function! TabRename(tablabel) abort
  let t:tablabel = a:tablabel
  execute 'redrawtabline'
endfunction

if exists('+showtabline')
  command! -nargs=1 TabRename call TabRename(<q-args>)
  set tabline=%!TabLine()
endif

" Add Escape register. Usefull to search expresssion with interpreted as regex

function! EscapeClipboardRegister()
  let content = getreg('+')
  let @e = substitute(content, '[[:punct:]]', '.', 'g')
endfunction

" Replace vim register by tmux clipboard

function! TmuxToVimRegister(register)
  let l:tmux_content = system('tmux show-buffer')
  call setreg(a:register, l:tmux_content)
endfunction

" Add no CR register. Usefull to paste in visual mode.

function! SetVisualPasteRegister(mode, reg)
  if !empty($TMUX) && a:reg == '"'
    call TmuxToVimRegister(a:reg)
  end

  let l:reg = a:reg " Use the register passed as an argument
  if a:mode == 1
    normal "_d
    let @n = substitute(getreg(l:reg), '\n\?$', '\n', '')
  else
    let @n = substitute(getreg(l:reg), '\n$', '', '')
  endif
endfunction

" Remove empty line(s) at the end of the current file

function TrimEndTrailingLines()
  let lastLine = line('$')
  let lastNonblankLine = prevnonblank(lastLine)
  if lastLine > 0 && lastNonblankLine != lastLine
    silent! execute lastNonblankLine + 1 . ',$delete _'
  endif
endfunction

" Toggle quickfix list

function! ToggleQuickFixList()
  let current_tab = tabpagenr()
  let quickfix_open = !empty(filter(getwininfo(), {_, v -> v.quickfix && v.tabnr == current_tab}))
  if quickfix_open
    cclose
  else
    botrigh copen
  endif
endfunction

function! ToggleLocationList()
  let current_tab = tabpagenr()
  let quickfix_open = !empty(filter(getwininfo(), {_, v -> v.quickfix && v.tabnr == current_tab}))
  if quickfix_open
    lclose
  else
    botrigh lopen
  endif
endfunction

" Set Toggle colorcolumn

function! ColorColumn()
  if &colorcolumn == ""
    set colorcolumn=100
  else
    set colorcolumn=
  endif
endfunction

"}}}

"==============================================================================
" Remaps
"==============================================================================

"{{{

" Bracket Remap  ---------------------------------------------------------------

nnoremap [B  :blast<CR>
nnoremap [F  :clast<CR>
nnoremap [L  :lfirst<CR>
nnoremap [T  :tlast<CR>
nnoremap ]B  :bfirst<CR>
nnoremap ]F  :cfirst<CR>
nnoremap ]L  :llast<CR>
nnoremap ]Q  :cfirst<CR>
nnoremap ]T  :tfirst<CR>

nnoremap [b  :bp<CR>
nnoremap [f  :cp<CR>zz
nnoremap [l  :lprevious<CR>
nnoremap [t  :pop<CR>zz
nnoremap [z  zkzz
nnoremap ]b  :bn<CR>
nnoremap ]f  :cn<CR>zz
nnoremap ]l  :lnext<CR>
nnoremap ]t  :ta<CR>zz
nnoremap ]z  zjzz

" Ergonomy --------------------------------------------------------------------

" Remap the leader key

let mapleader=" "

" Disable the mapping for F1, it's near escape on thinkpad, really annoying

 map <F1> <nop>

 " Remap q/Q to avoid bad key pressing

nnoremap qq q
nnoremap q  <Nop>
nnoremap Q <Nop>

" Scroll horizontally easily

nnoremap zh 30zh
nnoremap zl 30zl

" Quickly resize Window

nnoremap <A-h> :vertical resize -2<CR>
nnoremap <A-l> :vertical resize +2<CR>
nnoremap <A-j> :resize +2<CR>
nnoremap <A-k> :resize -2<CR>

nnoremap <A-left> :vertical resize -2<CR>
nnoremap <A-right> :vertical resize +2<CR>
nnoremap <A-down> :resize +2<CR>
nnoremap <A-up> :resize -2<CR>

" In visual mode I don't want to paste with mouse

vmap <MiddleMouse> y

" Easily save

nnoremap <C-s> :w<cr>

" Easily quit, ZQ is difficult to type and Q a is too dangerous remap

nnoremap <C-q> :q<CR>

" Open tag in a new tab

nnoremap <leader><C-]> viw"ty:tab ts <C-R>t<CR>
vnoremap <leader><C-]> "ty:tab ts <C-R>t<CR>

" Write as sudo

command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

" Easily pipe the all file

nnoremap !$ :%!

" Make clearing constant between all program

nnoremap <C-L> :noh<CR><C-L>

" Search inside the current selection

vnoremap g/ <esc>/\%V

" Stay on the current word while searching

nnoremap # #N
nnoremap * *N

" Tab motion

nnoremap T :tabnew <CR>
nnoremap <leader>< :tabm -1<CR>
nnoremap <leader>> :tabm +1<CR>

" Always center screen during navigation, it should be default

nnoremap <C-d> <C-d>M
nnoremap <C-f> <C-f>M
nnoremap <C-b> <C-b>M
nnoremap <C-u> <C-u>M
nnoremap n nzvzz
nnoremap N Nzvzz
nnoremap <PageUp> <C-b>M
nnoremap <PageDown> <C-f>M

" I really to not use this and sometimes press it by mistake.

nnoremap H <Nop>
nnoremap L <Nop>


" Register --------------------------------------------------------------------

" Write vim register to tmux buffer

if !empty($TMUX) && executable('xclip')
  " Yank in vim and tmux clipboard and system clipboard
  nnoremap gyy V"+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>
  nnoremap gY v$"+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>
  nnoremap gyip vip"+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>
  nnoremap gyiw viw"+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>
  nnoremap gyiW viW"+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>
  vnoremap gy "+y:silent!!tmux set-buffer "$(xclip -o -selection clipboard)"<CR>

  " TODO: yanking not working with sting starting with - !

  " Yank in vim and tmux clipboard
  nnoremap yy V"0y:call system('tmux set-buffer ' . shellescape(@"))<cr>
  nnoremap Y v$"0y:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap yip vip"0y:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap yiw viw"0y:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap yiW viW"0y:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  vnoremap y "0y:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  inoremap <C-r>" <esc>:call TmuxToVimRegister('"')<cr>a<C-r>"

  " Delete in vim and tmux clipboard
  nnoremap dd V""d:call system('tmux set-buffer ' . shellescape(@"))<cr>
  nnoremap D v$<left>""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap dip vip""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di( vi(""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di) vi)""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di[ vi[""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di] vi]""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di{ vi{""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap di} vi}""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap diw viw""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  nnoremap diW viW""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>
  vnoremap d ""d:silent call system('tmux set-buffer ' . shellescape(@"))<CR>

  " Cut in vim and tmux clipboard
  vnoremap x ""x:silent call system('tmux set-buffer ' . shellescape(@"))<CR>

else
  " Yank in vim and system clipboard
  nnoremap gY v$"+y
  nnoremap gyip vip"+y
  nnoremap gyiw viw"+y
  nnoremap gyiW viW"+y
  vnoremap gy "+y
endif

" Do not copy when just one char was cut

nnoremap x "_x

" Modify default paste register and connecting it with tmux if present
"  - If I select with v I want to remove the last cr from the " register.
"  - If I select with V I want to keep the last cr from the " register.
"  NB: For visual mode typing vif does not make visual selection line-wize. An extra V need to to be added

vnoremap <expr> p (mode() ==# 'V') ? '"_d:call SetVisualPasteRegister(1, "\"")<cr>"nP' : ':call SetVisualPasteRegister(0, "\"")<cr>gv"_c<C-r>n<esc>gvo<esc>'
vnoremap <expr> P (mode() ==# 'V') ? '"_d:call SetVisualPasteRegister(1, "\"")<cr><left>"nP' : '"_d:call SetVisualPasteRegister(0, "\"")<cr><left>"nP'

nnoremap p :call TmuxToVimRegister('"')<CR>""p
nnoremap P :call TmuxToVimRegister('"')<CR>""P

nnoremap gp "+p
nnoremap gP "+P
" This does not work, bad register is selected.
vnoremap <expr> gp (mode() ==# 'V') ? '"_d:call SetVisualPasteRegister(1, "+")<cr>"nP' : ':call SetVisualPasteRegister(0, "+")<cr>gv"_c<c-r>n<esc>gvo<esc>'
vnoremap <expr> gP (mode() ==# 'V') ? '"_d:call SetVisualPasteRegister(1, "+")<cr><left>"nP' : '"_d:call SetVisualPasteRegister(0, "+")<cr><left>"nP'

" Add escape register remap

nnoremap "ep :call EscapeClipboardRegister()<CR>"ep
vnoremap "ep <esc>:call EscapeClipboardRegister()<CR>gv"ep

cnoremap <C-R>e <C-R>=EscapeClipboardRegister()<CR><del><C-R>e
inoremap <C-R>e <C-R>=EscapeClipboardRegister()<CR><del><C-R>e

nnoremap "eP :call EscapeClipboardRegister()<CR>"eP
vnoremap "eP <esc>:call EscapeClipboardRegister()<CR>gv"eP

" Text-Object action -----------------------------------------------------------

" Quickly make a header or a line

nnoremap gL !!line<CR>
nnoremap gH !!header<CR>

" Simple TMUX REPL

nnoremap glh :.!repl <CR>
nnoremap glip !iprepl <CR>
nnoremap gliw yiw:silent exec "!repl <C-R>0"<CR>
nnoremap gll :.!repl <CR>
nnoremap glr :.!repl <CR>
vnoremap gl :!repl <CR>

" Substitute

nnoremap gsif vif:%s/
nnoremap gsh V:s/
nnoremap gsip vip:s/
nnoremap gsl V:s/
nnoremap gss :%s/
vnoremap gs :s/

" Global command

nnoremap gbif vif:%g/
nnoremap gbh V:g/
nnoremap gbip vip:g/
nnoremap gbl V:g/
nnoremap gbb :%g/
vnoremap gb :g/

" Sort

nnoremap goip vip:sort<cr>
vnoremap go :sort<CR>

" Quickly make a header or a line

nnoremap gL !!line<CR>
nnoremap gH !!header<CR>

" Keep consistancy ------------------------------------------------------------

" Make Y as it D or C

nnoremap Y v$hy

" Make zC work as it should

nnoremap zC zxzc

 " Make - react the same between vim and netrw

nnoremap - :Explore!<CR>
autocmd FileType * if &filetype != '' | nnoremap - :let @q=expand("%:t")<CR>:Explore!<CR>/<C-R>q<CR> | endif

" Toggle vims options ---------------------------------------------------------

nnoremap yoC  :call ColorColumn()<CR>
nnoremap yoc  :set cursorcolumn!<CR>
nnoremap yof  :set foldenable!<CR>
nnoremap yol  :set list!<CR>
nnoremap yom  <ESC>:exec &mouse!=""? "set mouse=" : "set mouse=nv"<CR>
nnoremap yon  :set number!<CR>
nnoremap yop  :set paste<CR>
nnoremap yor  :set relativenumber!<CR>
nnoremap yose :set spelllang=en<CR>
nnoremap yosf :set spelllang=fr<CR>
nnoremap yoss :set spell!<CR>
nnoremap yow  :set wrap!<CR>

"}}}

"==============================================================================
" Leader Remaps
"==============================================================================

"{{{

" Tmux's Splits for current buffer

nnoremap <silent> <leader>% :!tmux split-window -h -c %:p:h<tab> & <CR><CR>
nnoremap <silent> <leader>" :!tmux split-window -c %:p:h<tab> & <CR><CR>

" b -- Buffer --
"   D : Delete all buffers except this one
"   b : switch Buffer
"   d : Delete current buffer
"   t : current buffer in new tab

nnoremap <Leader>be  :%bd\|e#
nnoremap <Leader>bb  :b
nnoremap <leader>bd  :bd <CR>
nnoremap <leader>bt  :tabnew \| e# <CR>

" d -- Files --
"   g  : Grep
"   l  : Location grep
nnoremap <leader>dg :grep "" %:p:h<C-left><left><left>
nnoremap <leader>dl :lgrep "" %:p:h<C-left><left><left>
vnoremap <leader>dg  y:grep <C-R>0 %:p:h
vnoremap <leader>dl  y:lgrep <C-R>0 %:p:h

" d -- Directory
"   f  : find File in current directory
nnoremap <leader>df  :edit %:p:h<Tab><C-d>

" f -- Files --
"   Y  : Yank Full file's name
"   c  : Copy file's content
"   dg : Diff Get
"   do : Diff Off
"   dp : Diff Put
"   dt : Diff This
"   f  : find File in current file's folder
"   g  : Grep
"   l  : Location grep
"   y  : Yank file's name

nnoremap <leader>fY  :let @+=expand("%:p")<CR>
nnoremap <leader>fc  :%y+
nnoremap <leader>fdg :diffget<CR>
nnoremap <leader>fdp :diffput<CR>
nnoremap <leader>fg  :grep "" %<C-left><left>
nnoremap <leader>fl  :lgrep "" %<left><left><left>
nnoremap <leader>fy  :let @+=expand("%:t")<CR>
nnoremap <leader>lg  :lgrep "" %<left><left><left>
vnoremap <leader>fg  y:grep <C-R>0 %
vnoremap <leader>fl  y:lgrep <C-R>0 %

" F  : toggle quickFixlist

nnoremap <leader>F :call ToggleQuickFixList()<CR>

" L  : toggle Location list

nnoremap <leader>L :call ToggleLocationList()<CR>

" r -- register
"   r : display Register

nnoremap <leader>rr :reg<cr>

" p -- Project
"   T : Tagstack
"   d : find Directory
"   f : find File
"   g : Grep
"   l : Location grep
"   t : find Tags

nnoremap <Leader>pT :tags<CR>
nnoremap <Leader>pd :Explore **/
nnoremap <Leader>pf :find *
nnoremap <Leader>pt :tag
nnoremap <leader>pg :grep ""<left>
nnoremap <leader>pl :lgrep ""<left>
nnoremap <leader>ps :tags<cr>
vnoremap <leader>pg  y:grep '<C-R>0'
vnoremap <leader>pl  y:lgrep '<C-R>0'

" w --- Windows --
"   Same remap as <C-w> but withou Ctrl. Easier to type.

nnoremap <Leader>w<down> <C-w><down>
nnoremap <Leader>w<left> <C-w><left>
nnoremap <Leader>w<right> <C-w><right>
nnoremap <Leader>w<up> <C-w><up>
nnoremap <Leader>w= <C-w>=
nnoremap <Leader>wH <C-w>H
nnoremap <Leader>wJ <C-w>J
nnoremap <Leader>wK <C-w>K
nnoremap <Leader>wL <C-w>L
nnoremap <Leader>wT <C-w>T
nnoremap <Leader>wh <C-w>h
nnoremap <Leader>wj <C-w>j
nnoremap <Leader>wk <C-w>k
nnoremap <Leader>wl <C-w>l
nnoremap <Leader>wo <C-w>o
nnoremap <Leader>ws <C-w>s
nnoremap <Leader>wv <C-w>v
nnoremap <Leader>ww <C-w>w
nnoremap <Leader>wx <C-w>x

"}}}

"==============================================================================
"  Autocmds and FileTypes
"-=============================================================================

"{{{

" Autocmd --------------------------------------------------------------------

" Return to last edited position after opening a file
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Remove trailing white space  and trailing line at save

autocmd BufWrite *  :%s/\s\+$//e
autocmd BufWritePre <buffer> call TrimEndTrailingLines() " Do not work on exs file

" Clear the jumplist at Vim opening, don't want previous jumps

autocmd VimEnter * clearjumps

" Set spell depending on the filetype

autocmd FileType * setlocal spell spelllang=en
autocmd FileType markdown setlocal spell spelllang=fr

" Filetype -------------------------------------------------------------------

" C

autocmd FileType c setlocal path+=/usr/include

" Makefile

autocmd FileType make setlocal noexpandtab

" Markdown

autocmd FileType markdown setlocal norelativenumber
autocmd FileType markdown setlocal nonu
autocmd FileType markdown setlocal foldlevel=0
autocmd FileType markdown setlocal conceallevel=2 " conceal markdown link

" Netrw

autocmd FileType netrw nnoremap <buffer> <leader>% :let $VIM_DIR=b:netrw_curdir<CR>:!tmux split-window -h -c $VIM_DIR <CR><CR>
autocmd FileType netrw nnoremap <buffer> <leader>" :let $VIM_DIR=b:netrw_curdir<CR>:!tmux split-window  -c $VIM_DIR <CR><CR>
autocmd FileType netrw nnoremap <buffer><C-L> <Nop>
autocmd FileType netrw nnoremap <buffer><C-L> :noh<CR>:Explore<CR>

" Shell

autocmd FileType sh setlocal noexpandtab " Keep tabulation in shell script for EOF syntax

" Vim

autocmd FileType vim setlocal foldmethod=marker
autocmd BufRead,BufNewFile init.lua setlocal foldmethod=marker

"}}}

"==============================================================================
" Source Local Configuration
"==============================================================================

"{{{

" Source local configuration

if !empty(glob("~/.vim/local_configuration.vim"))
    source ~/.vim/local_configuration.vim
endif

"}}}
