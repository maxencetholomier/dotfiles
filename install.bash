#!/bin/bash

# Work for Debian or Ubuntu

#===============================================================================
#  Variables
#===============================================================================

INSTALL="sudo -E apt -y install"
UPDATE="sudo -E apt update"
UNAME=$(grep -w ID /etc/os-release | cut -d'=' -f2)

if [[ ${UNAME} == "debian" ]]; then
	TEXLIVE="texlive-latex-full"
	"${INSTALL}" flatpak
	"${INSTALL}" gnome-software-plugin-flatpak
	flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
	INSTALL_GRAPHICAL="flatpak install"
elif [[ ${UNAME} == "debian" ]]; then
	TEXLIVE="texlive-full"
	INSTALL_GRAPHICAL="snap install"
else
	echo "Installation only available on Debian and Ubuntu"
	exit 1
fi

#===============================================================================
# Create Folder
#===============================================================================

# Files and Folders Management -------------------------------------------------
[ -e ~/.bashrc ] && rm ~/.bashrc
[ -e ~/.profile ] && rm ~/.profile
[ -e ~/.vimrc ] && rm ~/.vimrc
[ -e ~/Bureau ] && rmdir ~/Bureau
[ -e ~/Documents ] && rmdir ~/Documents
[ -e ~/Images ] && rmdir ~/Images
[ -e ~/Modèles ] && rmdir ~/Modèles
[ -e ~/Musiques ] && rmdir ~/Musiques
[ -e ~/Publics ] && rmdir ~/Public
[ -e ~/Téléchargements ] && rmdir ~/Téléchargements
[ -e ~/Vidéos ] && rmdir ~/Vidéos

mkdir ~/bureau
mkdir ~/logiciels
mkdir ~/remoterepos
mkdir ~/remoterepos/projects
mkdir ~/telechargements
mkdir ~/tmp
mkdir ~/.go

export GOPATH="${HOME}/.go"

# Create Git Folders -----------------------------------------------------------
git clone git@gitlab.com:maxencetholomier/tools ~/remoterepos/
git clone git@gitlab.com:maxencetholomier/zettelkasten.git ~/remoterepos/
git clone git@gitlab.com:maxencetholomier/knowledge.git ~/remoterepos/projects
sudo ~/remoterepos/projects/knowledge/install.sh

#===============================================================================
# Installation
#===============================================================================

$UPDATE

# Network ----------------------------------------------------------------------
"${INSTALL}" ssh
"${INSTALL}" curl

# File Management --------------------------------------------------------------
"${INSTALL}" stow
"${INSTALL}" git

# Monitoring
"${INSTALL}" btop
"${INSTALL}" htop

# Documents conversion ---------------------------------------------------------
"${INSTALL}" "${TEXLIVE}"
"${INSTALL}" pandoc
"${INSTALL}" pdf2svg
"${INSTALL}" pdflatex

# Image analysis ---------------------------------------------------------------
"${INSTALL}" tesseract-ocr # Needed for my OCR script
"${INSTALL}" libtesseract-dev # Needed for tesseract-ocr

# Development ------------------------------------------------------------------
"${INSTALL}" bash-completion
"${INSTALL}" bat
"${INSTALL}" cargo # Needed for some linters
"${INSTALL}" entr
"${INSTALL}" lazygit
"${INSTALL}" nvim
"${INSTALL}" python3-pip # Needed for some linters
"${INSTALL}" ranger
"${INSTALL}" ripgrep
"${INSTALL}" tmux
"${INSTALL}" universal-ctags
"${INSTALL}" xclip
[[ ${UNAME} =~ (ubuntu|debian) ]] && ln -s /usr/bin/batcat ~/.local/bin/bat

# Diffastic
cargo install --locked difftastic

# FZF
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# Kitty
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
sed -i "s|Icon=kitty|Icon=$(readlink -f ~)/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=$(readlink -f ~)/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
echo 'kitty.desktop' > ~/.config/xdg-terminals.list

# Lazygit
go install github.com/jesseduffield/lazygit@latest

# Ov Pager
go install github.com/noborus/ov@master

# Vim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# Zoxide
cargo install zoxide --locked

# Displaying image preview in NVIM in TMUX
"${INSTALL}" imagemagick
"${INSTALL}" libmagickwand-dev
"${INSTALL}" luarocks
luarocks install magick

# Displaying video preview in ranger
"${INSTALL}" ffmpegthumbnailer

# Media ------------------------------------------------------------------------
[[ ${UNAME} == "debian" ]] && "${INSTALL}" flatpak

"${INSTALL_GRAPHICAL}" firefox
"${INSTALL_GRAPHICAL}" gimp
"${INSTALL_GRAPHICAL}" mpv # preview sound on ranger
"${INSTALL_GRAPHICAL}" vlc
"${INSTALL}" xournalpp

#===============================================================================
# END
#===============================================================================

echo "Installation finished"
echo "	Don't forget to installed manually Ueberzug++ !"
echo "	Don't forget to installed manually configure Joplin"
