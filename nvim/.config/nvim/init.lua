vim.cmd("source ~/.vimrc")

-- Basic Option on top of vim

-- Important to focus on text only
DIAGNOSTICS_ACTIVE = true
local toggle_diagnostics = function()
  DIAGNOSTICS_ACTIVE = not DIAGNOSTICS_ACTIVE
  if DIAGNOSTICS_ACTIVE then
    print("Diagnostic enable")
    vim.diagnostic.enable(true)
  else
    print("Diagnostic disable")
    vim.diagnostic.enable(false)
  end
end

vim.keymap.set("n", "yod", toggle_diagnostics)

-- Vital for LSP and Tagbar on Neovim
vim.o.updatetime = 500

-- TODO: there is snake and camel case here. Chose one and correct.
-- TODO: kill my vimrc configuration
-- TODO: configure foldmethod with treesitter
--      vim.opt.foldmethod = "expr"
--      vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
--
--------------------------------------------------------------------------------
-- Plug-in Installation
--------------------------------------------------------------------------------

local vim = vim
local Plug = vim.fn["plug#"]

vim.call("plug#begin")

-- {{{

-- AI
-- TODO: Make custom prompt to correct french spelling mistake
Plug("David-Kunz/gen.nvim")

-- AST
Plug("nvim-treesitter/nvim-treesitter", { ["do"] = ":TSUpdate" })
Plug("nvim-treesitter/nvim-treesitter-textobjects")

-- Completion
Plug("hrsh7th/cmp-buffer")
Plug("hrsh7th/cmp-cmdline")
Plug("hrsh7th/cmp-nvim-lsp")
Plug("hrsh7th/cmp-path")
Plug("hrsh7th/nvim-cmp")
Plug("saadparwaiz1/cmp_luasnip")

-- Git
Plug("tpope/vim-fugitive")
Plug("airblade/vim-gitgutter")

-- Navigation (Files, Windows, Buffers, Tags, ...)
-- Plug("ibhagwan/fzf-lua")
Plug("ibhagwan/fzf-lua", { ["commit"] = "c46eb14" }) -- wait until tmux popup work again
Plug("preservim/tagbar")
Plug("szw/vim-maximizer")

-- LSP, Linter, Formatter and  DAP
Plug("mfussenegger/nvim-lint")
Plug("neovim/nvim-lspconfig")
Plug("stevearc/conform.nvim")
Plug("williamboman/mason-lspconfig.nvim")
Plug("williamboman/mason.nvim")

-- Snippet
Plug("L3MON4D3/LuaSnip", { ["tag"] = "v2.*", ["do"] = "make install_jsregexp" })
Plug("honza/vim-snippets")

-- Theme and GUI/TUI
Plug("3rd/image.nvim", { ["for"] = "markdown" })
Plug("morhetz/gruvbox")
-- Plug("iamcco/markdown-preview.nvim", { ["do"] = "cd app && npx --yes yarn install", ["for"] = "markdown" })
Plug("Carus11/markdown-preview.nvim", { ["do"] = "cd app && npx --yes yarn install", ["for"] = "markdown" }) -- wait until markdown preview merge this branch
Plug("norcalli/nvim-colorizer.lua")

-- Text Editing
Plug("dhruvasagar/vim-table-mode", { ["for"] = { "markdown", "txt" } })
Plug("tpope/vim-surround")
Plug("tpope/vim-repeat")
Plug('fatih/vim-go', { ['do'] = ':GoUpdateBinaries', ["for"] = "go" })

-- Diff
Plug('AndrewRadev/linediff.vim')
Plug('will133/vim-dirdiff')

-- }}}

vim.call("plug#end")

--------------------------------------------------------------------------------
-- Conform
--------------------------------------------------------------------------------

-- {{{

-- Format after SAVING !!
-- It will format the old version if you don't.
--
if vim.g.plugs["conform.nvim"] then
  local conform = require("conform")

  conform.setup({
    formatters = {
      mix = { args = { "format", "-" } }, -- Override mix args if mix version is 1.13 or lower
    },
    formatters_by_ft = {
      elixir = { "mix" },
      css = { "prettier" },
      html = { "prettier" },
      htmldjango = { "djlint" },
      javascript = { "prettier" },
      json = { "prettier" },
      lua = { "stylua" },
      markdown = { "prettier" },
      python = { "black" },
      sh = { "shfmt" },
      xml = { "xmlformat" },
    },
  })

  vim.o.formatexpr = "v:lua.require('conform').formatexpr({'timeout_ms':2000})"
end

-- }}}

--------------------------------------------------------------------------------
-- Cmp-Nvim
--------------------------------------------------------------------------------

--{{{

if vim.g.plugs["nvim-cmp"]
    and vim.g.plugs["cmp-cmdline"]
    and vim.g.plugs["cmp-nvim-lsp"]
    and vim.g.plugs["cmp-buffer"] then
  local cmp = require 'cmp'

  cmp.setup({
    snippet = {
      expand = function(args)
        require("luasnip").lsp_expand(args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert({
      ["<CR>"] = cmp.mapping.confirm({ select = true }),
      -- ["<C-Y>"] = cmp.mapping.confirm({ select = true }),
      ["<Tab>"] = function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        else
          fallback()
        end
      end,
      ["<-S-Tab>"] = function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        else
          fallback()
        end
      end,
    }),
    sources = cmp.config.sources({
      { name = "nvim_lsp" },
      { name = "luasnip" },
      { name = "path" },
      { name = "buffer" },
    }),

    enabled = function()
      local context = require("cmp.config.context")
      if vim.api.nvim_get_mode().mode == "c" then
        return true
      else
        return not context.in_treesitter_capture("comment") and not context.in_syntax_group("Comment")
      end
    end,
  })

  cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline({}),

    sources = {
      { name = "buffer" },
    },
  })

  cmp.setup.cmdline(":", {

    -- It's rewritting the default vim behavior, why does they do that ?
    --   C-p is used to search for the previous item
    --   up is used to search for the previous item that start with lie the current command
    mapping = cmp.mapping.preset.cmdline({
      ["<C-n>"] = {
        c = false,
      },
      ["<C-p>"] = {
        c = false,
      },
    }),

    sources = cmp.config.sources({
      { name = "path" },
      { name = "buffer" },
      { name = "cmdline" },
    }),
  })
end

--}}}

--------------------------------------------------------------------------------
-- Fugitive
--------------------------------------------------------------------------------

-- {{{

-- Option -----------------------------------------------------------------------

if vim.g.plugs["vim-fugitive"] then
  -- Enable fold in fugitive's buffer
  vim.cmd([[autocmd FileType git setlocal foldmethod=syntax]])

  -- Enable branch in status bar
  vim.cmd([[ set statusline=%<%f\ %h%m%r%=%{FugitiveStatusline()}\ \ \ \ \ \ %-14.(%l,%c%V%)\ %P ]])
end

-- Remap ------------------------------------------------------------------------

if vim.g.plugs["vim-fugitive"] then
  FUGITIVE_PREVIOUS_BUFFER = ""
  -- local function fugitive_vert_log()
  --   FUGITIVE_PREVIOUS_BUFFER = vim.fn.expand("%")
  --   vim.cmd("vert G log --oneline -- %")
  -- end

  -- local get_first_element = function(string, separator)
  --   local output = ""
  --   for word in string.gmatch(string, "[^" .. separator .. "]+") do
  --     output = word
  --     break
  --   end
  --   return output
  -- end

  -- local function fugitive_edit_file()
  --   vim.api.nvim_command('normal! "qy')
  --   local commit_name = get_first_element(vim.fn.getreg("q"), ":")
  --   if FUGITIVE_PREVIOUS_BUFFER then
  --     vim.cmd("Gedit " .. commit_name)
  --   end
  -- end

  -- local function fugitive_vert_diff_file()
  --   vim.api.nvim_command('normal! V')
  --   vim.api.nvim_command('normal! "qy')
  --   local commit_name = get_first_element(vim.fn.getreg("q"), " ")
  --   if FUGITIVE_PREVIOUS_BUFFER then
  --     vim.cmd("Gedit " .. commit_name .. ":" .. FUGITIVE_PREVIOUS_BUFFER)
  --     vim.cmd("windo diffthis")
  --     vim.api.nvim_input("<C-W>h")
  --   end
  -- end

  -- local function fugitive_vert_edit_file()
  --   vim.api.nvim_command('normal! "qy')
  --   local commit_name = get_first_element(vim.fn.getreg("q"), " ")
  --   if FUGITIVE_PREVIOUS_BUFFER then
  --     vim.cmd("Gedit " .. commit_name .. ":" .. FUGITIVE_PREVIOUS_BUFFER)
  --     vim.cmd("only")
  --   end
  -- end

  local remap_option = { noremap = true, silent = false }

  -- g -- Git --
  vim.api.nvim_set_keymap("n", "<Leader>gd", ":Gvdiffsplit !^", remap_option)
  vim.api.nvim_set_keymap("n", "<Leader>ge", ":Gedit ", remap_option)
  -- TODO: make a function to search the file while typing <leader>gg
  -- vim.api.nvim_set_keymap("n", "<Leader>gg", ":let @f=expand('%:.')<CR>:G <CR>/\\V<C-r>f<cr>", remap_option)
  vim.api.nvim_set_keymap("n", "<Leader>gg", ":G <CR>", remap_option)
  vim.api.nvim_set_keymap("n", "<Leader>glG", ":Gclog --all -G''<left>", remap_option)      -- Search inside commit
  vim.api.nvim_set_keymap("n", "<Leader>glg", ':Gclog --all --grep=""<left>', remap_option) -- Search commit name
  vim.api.nvim_set_keymap("n", "<Leader>gll", ":G log -1000", remap_option)                 -- Search inside commit
  vim.api.nvim_set_keymap("n", "<Leader>gm", ":G blame <CR>", remap_option)
  vim.api.nvim_set_keymap("n", "<Leader>gr", ":Ggrep", remap_option)
  vim.api.nvim_set_keymap("n", "<leader>gb", ":G branch", remap_option)
  vim.api.nvim_set_keymap("n", "<leader>gcl", ":Gclog", remap_option)
  vim.api.nvim_set_keymap("v", "<leader>gl", ":Gclog", remap_option)
  -- vim.api.nvim_set_keymap("v", "<leader>gll", ":Gclog<CR>", remap_option)

  vim.api.nvim_set_keymap("n", "<leader>glf", "<cmd>lua fugitive_vert_log()<CR>", remap_option)
  vim.api.nvim_set_keymap("v", "<Leader>ge", "<cmd>lua fugitive_edit_file()<CR>", remap_option)

  -- f -- Files --
  vim.api.nvim_set_keymap("n", "<leader>fdh", ":diffget //2 <CR>", remap_option)
  vim.api.nvim_set_keymap("n", "<leader>fdl", ":diffget //3 <CR>", remap_option)

  -- Interact with the opened buffer <leader>glf
  -- TODO: save the line and try to go to the were glf has been called
  -- TODO: should I really need this two remap ?
  -- "vim.cmd([[autocmd FileType git nnoremap <buffer> <leader>gd V:lua fugitive_vert_diff_file()<CR> ]])
  -- vim.cmd([[autocmd FileType git nnoremap <buffer> <leader>ge V:lua fugitive_vert_edit_file()<CR>)<CR> ]])

  -- TODO: add git-jump script integration

  -- Remap like co<space>
  vim.cmd([[autocmd FileType fugitive nnoremap <buffer> cp<space> <ESC>:G cherry-pick ]])
  vim.cmd([[autocmd FileType fugitive nnoremap <buffer> m<space> <ESC>:G cherry-pick ]])
  vim.cmd([[autocmd FileType fugitive nnoremap <buffer> cb<space> <ESC>:G checkout -b ]])

  -- Keep this remaps persitant between vim buffer and fugitive buffer
  vim.cmd([[autocmd FileType fugitive nnoremap <silent><buffer><leader>% :!tmux split-window -h <CR>:clear<CR>]])
  vim.cmd([[autocmd FileType fugitive nnoremap <silent><buffer><leader>" :!tmux split-window -c <CR>:clear<CR>]])

  -- Make this remap persistant between the buffer
  vim.cmd([[
    autocmd FileType fugitive nnoremap <buffer> - :Explore . <CR>
  ]])
end

-- }}}

--------------------------------------------------------------------------------
-- FZF-Lua
--------------------------------------------------------------------------------

--{{{

if vim.g.plugs["fzf-lua"] then
  -- Classical fzf functions --------------------------------------------------

  local fzf_path_exclusions = os.getenv("FZF_EXCLUSION_LIST")
  local gnome_theme = ""

  if vim.env.XDG_CURRENT_DESKTOP and vim.env.XDG_CURRENT_DESKTOP:match("GNOME") then
    local handle = io.popen("gsettings get org.gnome.desktop.interface color-scheme")
    if handle then
      gnome_theme = handle:read("*a"):gsub("%s+", "")
      handle:close()
      vim.env.GNOME_THEME = gnome_theme
    end
  end

  if gnome_theme:match("dark") then
    vim.env.BAT_THEME = "gruvbox-dark"
    vim.env.FZF_DEFAULT_OPTS =
    "--color=preview-border:#727272,border:#727272,separator:#ebdbb2,hl+:#d11010,hl:#d11010  --bind 'ctrl-]:transform-query:echo -n {q}; tmux show-buffer'"
  else
    vim.env.BAT_THEME = "gruvbox-light"
    vim.env.FZF_DEFAULT_OPTS =
    "--color=hl+:#d11010,hl:#d11010,bg+:#ddd3ac,fg+:#000000,border:#292929 --bind 'ctrl-]:transform-query:echo -n {q}; tmux show-buffer'"
  end

  local fzf_option_theme = {}
  fzf_option_theme = {
    ["--layout"] = "default",
    ["--preview-window"] = "top:wrap",
  }

  local preview_option = {
    no_header = true,
    multiprocess = true,
    git_icons = false,
    file_icons = false,
    color_icons = false,
  }

  local fzf_env = ""
  if os.getenv("TMUX") then
    fzf_env = "fzf-tmux"
  else
    fzf_env = "default"
  end

  require("fzf-lua").setup({
    fzf_env,
    fzf_tmux_opts = {
      ["-p"] = "90%,80%",
    },
    fzf_opts = fzf_option_theme,
    files = preview_option,
    grep = preview_option,
    buffers = preview_option,
    previewers = {
      tree = {
        cmd = "tree",
      }
    }
  })

  -- TODO: Refacto all these functions
  function Files(cwd)
    cwd = cwd or vim.fn.getcwd()
    require('fzf-lua').files({
      cmd = "find . -type f | grep -v '" .. fzf_path_exclusions .. "'",
      cwd = cwd,
    })
  end

  -- TODO: Replace tree by ls ?
  function Zoxide()
    require("fzf-lua").files({
      cmd = "zoxide query -l",
      fzf_opts = {
        ['--preview'] = "tree -C -L 1 {}",
      },
      previewer = "tree"
    })
  end

  function Directories(cwd)
    cwd = cwd or vim.fn.getcwd()
    require("fzf-lua").files({
      cmd = "find . -type d | grep -v '" .. fzf_path_exclusions .. "'",
      cwd = cwd,
      fzf_opts = {
        ['--preview'] = "tree -C -L 1 {}",
      },
      previewer = "tree"
    })
  end

  -- The default function from the plugin is unusable with my fzf opts.
  --   Do I need to differentiate stage and untracked files ?
  function GitCmd(cmd)
    if io.open(io.popen("pwd"):read() .. "/.git", "r") then
      require("fzf-lua").files({
        cmd = cmd
      })
    else
      print("Not a Git repository")
    end
  end

  function IsNetrwBuffer()
    if vim.b.netrw_localrm ~= nil then
      return true
    end

    if vim.bo.filetype == 'netrw' then
      return true
    end

    return false
  end

  function DirectoryLocal()
    local current_file_dir
    if IsNetrwBuffer then
      current_file_dir = vim.b.netrw_curdir
    else
      current_file_dir = vim.fn.expand("%:p:h")
    end
    Directories(current_file_dir)
  end

  function FilesLocal()
    local current_file_dir
    if IsNetrwBuffer then
      current_file_dir = vim.b.netrw_curdir
    else
      current_file_dir = vim.fn.expand("%:p:h")
    end
    Files(current_file_dir)
  end

  function GrepLocal()
    local current_file_dir
    if IsNetrwBuffer then
      current_file_dir = vim.b.netrw_curdir
    else
      current_file_dir = vim.fn.expand("%:p:h")
    end
    require('fzf-lua').grep_project({ cwd = current_file_dir })
  end

  -- Project
  vim.keymap.set("n", "<leader>pd", ":lua Directories()<CR>")
  vim.keymap.set('n', '<leader>pf', ":lua Files()<CR>")

  vim.keymap.set("n", "<leader>ph", ":lua require('fzf-lua').oldfiles()<CR>")
  vim.keymap.set("n", "<leader>pk", ":lua require('fzf-lua').tagstack()<CR>")
  vim.keymap.set("n", "<leader>pr", ":lua require('fzf-lua').grep_project()<CR>")
  vim.keymap.set("n", "<leader>pt", ":lua require('fzf-lua').tags()<CR>")

  -- Current File
  vim.keymap.set("n", "<leader>ft", ":lua require('fzf-lua').btags()<CR>")
  vim.keymap.set("n", "<leader>fr", ":lua require('fzf-lua').lines()<CR>")
  vim.keymap.set("n", "<leader>fh", ":lua require('fzf-lua').oldfiles()<CR>")

  -- Current Directory
  vim.keymap.set("n", "<leader>dd", ":lua Directories(vim.fn.expand(\"%:p:h\"))<CR>")
  vim.keymap.set('n', '<leader>df', FilesLocal)
  vim.keymap.set("n", "<leader>dr", GrepLocal)

  -- Buffer
  vim.keymap.set("n", "<leader>bb", ":lua require('fzf-lua').buffers()<CR>")
  vim.keymap.set("n", "<leader>br", ":lua require('fzf-lua').lines()<CR>")

  -- Git
  vim.keymap.set("n", "<leader>gwh", ":lua GitCmd('git show --pretty=\"format:\" --name-only HEAD')<CR>")
  vim.keymap.set("n", "<leader>gwH", ":lua GitCmd('git show --pretty=\"format:\" --name-only HEAD~1')<CR>")
  vim.keymap.set("n", "<leader>gs", ":lua GitCmd(\"git status --short | awk '{print $2}'\")<CR>")

  -- LSP
  vim.keymap.set("n", "<leader>lws", ":lua require('fzf-lua').lsp_workspace_symbols()<CR>")

  -- Zoxide
  vim.keymap.set("n", "<leader>o", Zoxide)

  -- Zettelkasten -------------------------------------------------------------

  local zet_dir = os.getenv("zettelkasten")
  local zet_dir_company = os.getenv("zettelkasten_company")

  function KFiles(cwd)
    local opts = {}
    opts.preview = "bat -p --theme=${BAT_THEME} --color=always {1}.md "
    opts.cwd = cwd
    opts.fzf_opts = {
      ["--with-nth"] = "3.."
    }
    opts.actions = {
      ["ctrl-t"] = function(selected)
        for i = 1, #selected do
          local filename = string.gsub(selected[i], " .*", "")
          vim.cmd("tabnew " .. cwd .. "/" .. filename .. ".md")
        end
      end,
      ["ctrl-v"] = function(selected)
        for i = 1, #selected do
          local filename = string.gsub(selected[i], " .*", "")
          vim.cmd("vert new | Explore " .. cwd .. "/" .. filename .. ".md")
        end
      end,
      ["ctrl-s"] = function(selected)
        for i = 1, #selected do
          local filename = string.gsub(selected[i], " .*", "")
          vim.cmd("new | Explore " .. cwd .. "/" .. filename .. ".md")
        end
      end,
      default = function(selected)
        for i = 1, #selected do
          local filename = string.gsub(selected[i], " .*", "")
          vim.cmd("e " .. cwd .. "/" .. filename .. ".md")
        end
      end,
    }
    if cwd == zet_dir then
      opts.prompt = "Zet> "
      require("fzf-lua").fzf_exec('k l', opts)
    else
      opts.prompt = "ZetCompany> "
      require("fzf-lua").fzf_exec('c l', opts)
    end
  end

  function KNew(cwd)
    local timestamp = os.date("%Y%m%d%H%M%S")
    local filepath = cwd .. "/" .. timestamp .. ".md"
    local file = io.open(filepath, "w")

    if file then
      file:write("# " .. timestamp .. " : " .. "\n")
      file:close()
    end

    vim.cmd("e " .. filepath)
  end

  if zet_dir then
    vim.keymap.set("n", "<leader>kn", ":lua KNew('" .. zet_dir .. "')<cr>")
    vim.keymap.set("n", "<leader>kf", ":lua KFiles('" .. zet_dir .. "')<cr>")
    vim.keymap.set('n', '<leader>kr', ":lua require(\"fzf-lua\").grep_project({ cwd = \"" .. zet_dir .. "\", })<cr>")
  end

  if zet_dir_company then
    vim.keymap.set("n", "<leader>cn", ":lua KNew('" .. zet_dir_company .. "')<cr>")
    vim.keymap.set("n", "<leader>cf", ":lua KFiles('" .. zet_dir_company .. "')<cr>")
    vim.keymap.set('n', '<leader>cr',
      ":lua require(\"fzf-lua\").grep_project({ cwd = \"" .. zet_dir_company .. "\", })<cr>")
  end

  -- Make my terminal script available in Nvim --------------------------------

  vim.api.nvim_create_user_command('Vid', function()
    require('fzf-lua').files({
      cmd = "find ${dotfiles} -type f | grep -v '" .. fzf_path_exclusions .. "'",
    })
  end, {})

  -- A (better?) way would be to do it in pure lua if I want to exclude binary files
  vim.api.nvim_create_user_command('Vic', function()
    require('fzf-lua').files({
      cmd = "for cmd in $(compgen -ac); do command -v \"${cmd}\";done ",
    })
  end, {})

  vim.api.nvim_create_user_command('Cdf', function()
    require('fzf-lua').files({
      cmd = "zoxide query -l | grep -v '" .. fzf_path_exclusions .. "'",
      fzf_opts = {
        ['--preview'] = "tree -C -L 1 {}",
      },
      previewer = "tree"
    })
  end, {})
end

--}}}

--------------------------------------------------------------------------------
-- Gruvbox
--------------------------------------------------------------------------------

--{{{

if vim.g.plugs["gruvbox"] then
  local colorscheme_name = "gruvbox"
  vim.cmd("colorscheme " .. colorscheme_name)
  vim.cmd([[ hi Normal guibg=NONE ctermbg=NONE ]])
  vim.cmd([[ hi NonText guibg=NONE ctermbg=NONE ]])
end

--}}}

--------------------------------------------------------------------------------
-- Image
--------------------------------------------------------------------------------

--{{{

if vim.g.plugs["image.nvim"] then
  require("image").setup({
    backend = "kitty",
    integrations = {
      markdown = {
        enabled = true,
        clear_in_insert_mode = false,
        download_remote_images = true,
        only_render_image_at_cursor = false,
        filetypes = { "markdown" },
      },
      html = {
        enabled = false,
      },
      css = {
        enabled = false,
      },
    },
    max_width = nil,
    max_height = nil,
    max_width_window_percentage = nil,
    max_height_window_percentage = 50,
    window_overlap_clear_enabled = false,
    window_overlap_clear_ft_ignore = { "cmp_menu", "cmp_docs", "" },
    editor_only_render_when_focused = false,
    tmux_show_only_in_active_window = false,
    hijack_file_patterns = { "*.svg", "*.png", "*.jpg", "*.jpeg", "*.gif", "*.webp" },
  })
end

--}}}

--------------------------------------------------------------------------------
-- LSP
--------------------------------------------------------------------------------

--{{{

-- Mason -----------------------------------------------------------------------

if vim.g.plugs["mason.nvim"] and vim.g.plugs["mason-lspconfig.nvim"] then
  -- Linter to install manually
  -- djlint
  -- eslint_d
  -- jq
  -- ruff
  -- shellcheck
  -- Formatter to install manually
  -- black
  -- credo
  -- jq
  -- prettier
  -- shfmt
  -- style lua
  -- xmlformat

  require("mason").setup()
  require("mason-lspconfig").setup({
    ensure_installed = {
      "bashls",
      "cssls",
      "elixirls@0.10.0",
      "html",
      "jinja_lsp",
      "jsonls",
      "lua_ls",
      "marksman",
      "pyright",
      "ts_ls",
      -- "gopls",
      "lemminx",
    },
  })
end

-- LSP-Config ------------------------------------------------------------------

require("lspconfig").ts_ls.setup({})
require 'lspconfig'.gopls.setup {}
require("lspconfig").bashls.setup({})
require("lspconfig").cssls.setup({})
require("lspconfig").elixirls.setup({ cmd = { "elixir-ls" } })
require("lspconfig").html.setup({})
require("lspconfig").jsonls.setup({})
require("lspconfig").marksman.setup({})
require("lspconfig").pyright.setup({})
require("lspconfig").vimls.setup({})
require("lspconfig").lemminx.setup({})

require("lspconfig").lua_ls.setup({
  on_init = function(client)
    local path = client.workspace_folders[1].name
    if vim.loop.fs_stat(path .. "/.luarc.json") or vim.loop.fs_stat(path .. "/.luarc.jsonc") then
      return
    end

    client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
      runtime = {
        version = "LuaJIT",
      },
      -- Make the server aware of Neovim runtime files
      workspace = {
        checkThirdParty = false,
        library = {
          vim.env.VIMRUNTIME,
        },
      },
    })
  end,
  settings = {
    Lua = {},
  },
})

vim.keymap.set("n", "<space>le", vim.diagnostic.open_float)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
vim.keymap.set("n", "<space>ld", vim.diagnostic.setloclist)

vim.api.nvim_create_autocmd("LspAttach", {
  group = vim.api.nvim_create_augroup("UserLspConfig", {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

    local opts = { buffer = ev.buf }
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
    vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, opts)
    -- vim.keymap.set('n', '<space>lwa', vim.lsp.buf.add_workspace_folder, opts)
    -- vim.keymap.set('n', '<space>lwr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set("n", "<space>lwl", function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set("n", "<space>ld", vim.lsp.buf.type_definition, opts)
    vim.keymap.set("n", "<space>ln", vim.lsp.buf.rename, opts)
    vim.keymap.set({ "n", "v" }, "<space>la", vim.lsp.buf.code_action, opts)
    vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
    -- See nvim conform to format by text object
    vim.keymap.set('n', '<space>lf', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})

--}}}

--------------------------------------------------------------------------------
-- Luasnip
--------------------------------------------------------------------------------

-- {{{

if vim.g.plugs["LuaSnip"] then
  local ls = require("luasnip")
  require("luasnip.loaders.from_snipmate").lazy_load()

  vim.keymap.set({ "i", "s" }, "<C-J>", function()
    ls.jump(-1)
  end, { silent = true })

  vim.keymap.set({ "i", "s" }, "<C-K>", function()
    ls.jump(1)
  end, { silent = true })

  vim.keymap.set({ "i", "s" }, "<C-E>", function()
    if ls.choice_active() then
      ls.change_choice(1)
    end
  end, { silent = true })

  require("snippets_elixir")
end

-- }}}

---------------------------------------------------------------------------------
-- Nvim-colorizer
---------------------------------------------------------------------------------

-- {{{

if vim.g.plugs["nvim-colorizer.lua"] then
  vim.opt.termguicolors = true

  -- Attaches to every FileType mode
  require 'colorizer'.setup()

  require 'colorizer'.setup {
    'bashrc',
    'conf',
    'css',
    'html',
    'javascript',
    'lua',
  }
end

-- }}}

--------------------------------------------------------------------------------
-- Nvim-lint
--------------------------------------------------------------------------------

-- {{{

if vim.g.plugs["nvim-lint"] then
  require("lint").linters_by_ft = {
    elixir = { "credo" },
    javascript = { "eslint", "eslint_d" },
    json = { "jq" },
    python = { "ruff" },
    shell = { "shellcheck" },
    typescript = { "eslint", "eslint_d" },
  }

  vim.api.nvim_set_keymap(
    "n",
    "yot",
    '<cmd>lua require("lint").try_lint()<CR><cmd>echo "Linter enable"<CR>',
    { noremap = true, silent = true }
  )
end

-- }}}

--------------------------------------------------------------------------------
-- Maximizer
--------------------------------------------------------------------------------

-- {{{

if vim.g.plugs["vim-maximizer"] then
  vim.api.nvim_set_keymap("n", "<Leader>wm", ":MaximizerToggle<CR>", { noremap = true, silent = true })
end

-- }}}

-- ==============================================================================
--  Table-Mode
-- ==============================================================================

-- {{{
--
if vim.g.plugs["vim-table-mode"] then
  vim.g.table_mode_disable_mappings = 1
  vim.g.table_mode_disable_tableize_mappings = 1

  vim.api.nvim_set_keymap("n", "yoT", ":TableModeToggle<CR>", { noremap = true })
end
-- }}}

--------------------------------------------------------------------------------
-- Tagbar
--------------------------------------------------------------------------------

-- {{{

-- Add Support for Elixir

if vim.g.plugs["tagbar"] then
  vim.g.tagbar_type_elixir = {
    ctagstype = "elixir",
    kinds = {
      "p:protocols",
      "m:modules",
      "e:exceptions",
      "y:types",
      "d:delegates",
      "f:functions",
      "c:callbacks",
      "a:macros",
      "t:tests",
      "i:implementations",
      "o:operators",
      "r:records",
    },
    sro = ".",
    kind2scope = {
      p = "protocol",
      m = "module",
    },
    scope2kind = {
      protocol = "p",
      module = "m",
    },
    sort = 0,
    excludekinds = { "z:trash" },
  }

  vim.g.tagbar_ctags_bin = "/usr/bin/ctags-universal"

  vim.g.tagbar_sort = 0

  -- Space is already my leader Key and K make more sense
  vim.g.tagbar_map_showproto = "K"

  -- Differenciate the tags and the search colour
  vim.cmd("highlight TagbarHighlight guibg=#CCCCCC guifg=#000000")

  -- remap
  vim.api.nvim_set_keymap("n", "<leader>T", ":Tagbar<CR>", { noremap = true })
  vim.api.nvim_set_keymap("n", "]g", ':call tagbar#jumpToNearbyTag(1, "nearest")<CR>zz', { noremap = true })
  vim.api.nvim_set_keymap("n", "[g", ':call tagbar#jumpToNearbyTag(-1, "nearest")<CR>zz', { noremap = true })

  function _G.TagbarIsOpen()
    for _, win in ipairs(vim.api.nvim_list_wins()) do
      local buf = vim.api.nvim_win_get_buf(win)
      local bufname = vim.api.nvim_buf_get_name(buf)

      if string.find(bufname, "Tagbar") then
        return true
      end
    end
  end
end

-- }}}

--------------------------------------------------------------------------------
-- Treesitter
--------------------------------------------------------------------------------

-- {{{

if vim.g.plugs["nvim-treesitter"] and vim.g.plugs["nvim-treesitter-textobjects"] then
  require("nvim-treesitter.configs").setup({
    ensure_installed =
    {
      "bash",
      "c",
      "eex",
      "elixir",
      "heex",
      "javascript",
      "lua",
      "python",
      "vim",
      "vimdoc",
    }
    ,
    sync_install = false,
    auto_install = true,

    -- ignore_install = { "javascript" },

    highlight = {
      enable = true,
      disable = { "c" },
      additional_vim_regex_highlighting = false,
    },
    textobjects = {
      select = {
        enable = true,

        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = true,

        keymaps = {
          -- You can use the capture groups defined in textobjects.scm
          ["af"] = "@function.outer",
          ["ad"] = "@block.outer",
          ["if"] = "@function.inner",
          ["id"] = "@block.inner",
          ["ac"] = "@class.outer",
          ["ic"] = { query = "@class.inner", desc = "Select inner part of a class region" },
          ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
        },
        selection_modes = {
          ["@parameter.outer"] = "v", -- charwise
          ["@function.outer"] = "V",  -- linewise
          ["@class.outer"] = "<c-v>", -- blockwise
        },
        include_surrounding_whitespace = true,
      },
    },
  })
end

-- }}}
